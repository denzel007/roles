<?php

namespace App\Models;

use Phalcon\Mvc\Model;
/**
 * Permissions
 * Stores the permissions by profile
 */
class Permissions extends Model
{
    /**
     *
     * @var integer
     */
    public $id;
    /**
     *
     * @var integer
     */
    public $groupsId;
    /**
     *
     * @var string
     */
    public $resource;
    /**
     *
     * @var string
     */
    public $action;
    public function initialize()
    {
        $this->belongsTo('groupsId', __NAMESPACE__ . '\Groups', 'id', [
            'alias' => 'group'
        ]);
    }
}
