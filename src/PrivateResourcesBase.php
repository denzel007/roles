<?php

namespace Tetrapak07\Roles;

/**
 * Description of PrivateResourcesBase
 *
 * @author den
 */
class PrivateResourcesBase
{
    public static function getResources()
    {
        return [
        'crudgen' => [
            'index',
            'gen',
            'allModules',
            'genAll'
        ],
        'supermodule' => [
            'index',
            'allModules',
            'change', 
        ]
    ];
            
    }
}
