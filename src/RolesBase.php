<?php

namespace Tetrapak07\Roles;

use Phalcon\Mvc\Controller;
use Supermodule\ControllerBase as SupermoduleBase;
use Tetrapak07\FlashMessage\FlashMessage;

class RolesBase extends Controller
{
    public $dataReturn;
    
    public function initialize()
    {
       
    }
    
    public function onConstruct()
    {
        $this->dataReturn = true;
       
        if (!SupermoduleBase::checkAndConnectModule('roles')) {
            $this->dataReturn = false;
        } 
    }
    
    public function beforeExecuteRoute($dispatcher)
    {
        
        $controllerName = $dispatcher->getControllerName();
        $actionName = $dispatcher->getActionName();
        if (!$this->dataReturn) {
           $dispatcher->forward([
                            'controller' => $controllerName,
                            'action' => $actionName
                        ]);
           return false;
           
        }
      
        // Only check permissions on private controllers
        if ($this->acl->isPrivate($controllerName)) {
            
            // Get the current identity
            $identity = $this->auth->getIdentity();
            //print_r($identity);exit;
             $exeptArray = isset($this->config->modules->roles->exeptControllersActions) ? $this->config->modules->roles->exeptControllersActions->toArray() : [];
            //print_r($exeptArray);
            //echo $controllerName.'_'.$actionName;//exit;
            if (!is_array($identity) AND (!in_array($controllerName.'_'.$actionName, $exeptArray))) {
               
                $this->message('notice', 'You don\'t have access to this module: private');
                $role = isset( $this->auth->getIdentity()['groups'][0] ) ? $this->auth->getIdentity()['groups'][0] : 'guest';
                $redirPath =  $this->config->modules->authreg->successLoginRedirectRoles[$role];

                header("Location:". '/'.$redirPath ); 
                exit(); 
               
            }
            
             if (!is_array($identity) AND (in_array($controllerName.'_'.$actionName, $exeptArray))) {
                  return true;
             }
            // Check if the user have permission to the current option
           
            foreach ($identity['groups'] as $group) {
              
                if (!$this->acl->isAllowed($group, $controllerName, $actionName) /*AND (!in_array($controllerName.'_'.$actionName, $exeptArray))*/) {
                 
                    $this->message('notice', 'You don\'t have access to this module: ' . $controllerName . ':' . $actionName);
                    
                    
                    
                    if ($this->acl->isAllowed($group, $controllerName, $actionName) 
                            
                          // AND (!in_array($controllerName.'_'.$actionName, $exeptArray ))
                            ) {
                        $dispatcher->forward([
                            'controller' => $controllerName,
                            'action' => $actionName
                        ]);
                    } else {
                          $role = isset( $this->auth->getIdentity()['groups'][0] ) ? $this->auth->getIdentity()['groups'][0] : 'guest';
                          $redirPath =  $this->config->modules->authreg->successLoginRedirectRoles[$role];
                         if (in_array($group, $this->config->modules->roles->dontAdminInterfaceRoles->toArray()))  {
                           $this->auth->remove();
                         }
                         $this->message('notice', 'You don\'t have access to this module: private');
                         header("Location:". '/'.$redirPath); 
                         exit(); 
                    }

                    return false;
                }
            } 
            
        }
        
    }
    
    private function message($type = 'error', $mess = "Not found!", $dataUrl = '', $redirect = false)
    {
       $message = new FlashMessage();
       $message->initialize();
       return $message->message($type, $dataUrl, $mess, $redirect);
    }
     
} 